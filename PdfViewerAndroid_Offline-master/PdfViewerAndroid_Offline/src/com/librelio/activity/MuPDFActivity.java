package com.librelio.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.artifex.mupdf.MuPDFCore;
import com.artifex.mupdf.MuPDFPageAdapter;
import com.artifex.mupdf.view.DocumentReaderView;
import com.artifex.mupdf.view.ReaderView;

public class MuPDFActivity extends Activity
{
	private static final String TAG = "MuPDFActivity";
	private MuPDFCore core;
	private MuPDFCore cores[];
	private ReaderView docView;
	private MuPDFPageAdapter mDocViewAdapter;
	String paths[]={ Environment.getExternalStorageDirectory().getAbsolutePath()+ "/se.pdf"};

	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		cores= new MuPDFCore[paths.length];
		for(int i=0;i<paths.length;i++)
		{
			
			cores[i] = openFile(paths[i]);
			
		}
		createUI(savedInstanceState);
	}
	
	private void createUI(Bundle savedInstanceState) 
	{
		
		mDocViewAdapter = new MuPDFPageAdapter(this, cores);
		
		RelativeLayout layout = new RelativeLayout(this);
		LinearLayout bar = new LinearLayout(this);
		bar.setOrientation(LinearLayout.HORIZONTAL);
		
		
		// GoToPage Button
		final Button GoToBtn = new Button(this);
		final int end = mDocViewAdapter.getCount();
		
		docView = new DocumentReaderView(this)
		{
			@Override
			protected void onMoveToChild(View view, int i) 
			{
				super.onMoveToChild(view, i);
			}

			@Override
			public boolean onScroll(MotionEvent e1, MotionEvent e2,float distanceX, float distanceY)
			{
				boolean boo = super.onScroll(e1, e2, distanceX, distanceY);
				int currPage = getCurrentPage()+1;
				GoToBtn.setText(currPage+"/"+end);
				return boo;
			}

			@Override
			protected void onContextMenuClick() 
			{

			}

			@Override
			protected void onBuy(String path) 
			{
			
			}

		};
		docView.setAdapter(mDocViewAdapter);
		
		RelativeLayout.LayoutParams docViewParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.FILL_PARENT,
                RelativeLayout.LayoutParams.FILL_PARENT);
		docViewParams.topMargin = 80;
		
		layout.addView(docView,docViewParams);
		
		
		// Other buttons
		Button firstBtn = new Button(this);
		firstBtn.setText("<<");
		firstBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				docView.moveToPage(0);
				int currPage = docView.getCurrentPage()+1;
				GoToBtn.setText(currPage+"/"+end);
			}
		});
		bar.addView(firstBtn);
		Button prevBtn = new Button(this);
		prevBtn.setText("<");
		prevBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				docView.moveToPrevious();
				int currPage = docView.getCurrentPage()+1;
				GoToBtn.setText(currPage+"/"+end);
			}
		});
		bar.addView(prevBtn);
		
		
//		---------------------------------------------------------------------------		
		
		GoToBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				AlertDialog.Builder dialog = new AlertDialog.Builder(MuPDFActivity.this);
				dialog.setTitle("Go to page");
				final EditText input = new EditText(MuPDFActivity.this);
	            dialog.setView(input);
	            dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int whichButton) {
						String strPage = input.getEditableText().toString();
						int inputPage = 0;
						try{
							inputPage = Integer.parseInt(strPage);
						}
						catch (NumberFormatException ignore){}
						if((inputPage>=1)&&(inputPage<=end)){
							docView.moveToPage(inputPage-1);
							int currPage = docView.getCurrentPage()+1;
							GoToBtn.setText(currPage+"/"+end);
						}
					}
				});
				dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int whichButton) {
						dialog.cancel();
					}
				});
				AlertDialog alertDialog = dialog.create();
	        	alertDialog.show();
				
			}
		});
		bar.addView(GoToBtn);
		
		Button nextBtn = new Button(this);
		nextBtn.setText(">");
		nextBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				docView.moveToNext();
				int currPage = docView.getCurrentPage()+1;
				GoToBtn.setText(currPage+"/"+end);
			}
		});
		bar.addView(nextBtn);
		Button lastBtn = new Button(this);
		lastBtn.setText(">>");
		lastBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				docView.moveToPage(end-1);
				int currPage = docView.getCurrentPage()+1;
				GoToBtn.setText(currPage+"/"+end);
			}
		});
		bar.addView(lastBtn);
		final Button bookBtn = new Button(this);
		bookBtn.setText("*");
		bookBtn.setOnClickListener(new OnClickListener() {
			boolean bookmark = false;
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(bookmark == false){
					Toast.makeText(MuPDFActivity.this, 
						    "Bookmarked Successfully", Toast.LENGTH_LONG).show();
					bookmark = true;
				}
				else{
					Toast.makeText(MuPDFActivity.this, 
						    "Unbookmarked Successfully", Toast.LENGTH_LONG).show();
					bookmark = false;
				}
				
			}
		});
		bar.addView(bookBtn);
		
		LinearLayout.LayoutParams barParams = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);
		bar.setGravity(Gravity.CENTER_HORIZONTAL);
		
		layout.addView(bar,barParams);
//		---------------------------------------------------------------------------
		layout.setBackgroundColor(Color.BLACK);
		setContentView(layout);
	}
	

	private MuPDFCore openFile(String path) 
	{
		try
		{
			core = new MuPDFCore(path);
		} catch (Exception e) {
			Log.e(TAG, "get core failed", e);
			return null;
		}
		return core;
	}
}