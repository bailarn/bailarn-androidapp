package muict.bailarn.annotation;

import muict.bailarn.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;

public class DictionaryResult extends Activity{

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dictionary_result);

		Intent intent = getIntent();
		String keyword = (String)intent.getExtras().getString("keyword");
		WebView wv = (WebView)findViewById(R.id.webViewDict);
		wv.loadUrl("http://dict.longdo.com/mobile.php?search="+keyword);
	}
}
