package muict.bailarn.annotation;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class BookmarkManager {

	final String PREF_NAME = "bookmarks";
	final String key;
	SharedPreferences pref;
	ArrayList<Integer> bm;
	
	public BookmarkManager(Context c, String key){
		// Restore preferences
	    pref = c.getSharedPreferences(PREF_NAME, 0); //all bookmarks
		this.key = key; //material identifier
		loadBookmarks(); //load to bm
	}
	
	public void loadBookmarks(){
		bm = new ArrayList<Integer>();
		try {
			JSONArray j = new JSONArray(pref.getString(key, "[]"));
			for (int i = 0; i < j.length(); i++) {
		         bm.add(j.getInt(i));
		    }
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public boolean checkBookmark(int page){//TRUE = this page has been bookmarked, FALSE = otherwise
		if( bm.contains(page) ){
			return true;
		}
		return false;
	}
	
	public boolean setunset(int page){
		if(checkBookmark(page)){//now set -> change to unset
			bm.remove(new Integer(page)); //to avoid removeAt(page)
			//update shared pref
			updatePref();
			return false;//new status is unset
		}
		else{
			bm.add(page);
			updatePref();
			return true;
		}	
	}
	
	private void updatePref(){
		JSONArray jsonArray = new JSONArray();
		for(int i=0; i< bm.size(); i++){
			jsonArray.put(bm.get(i));
		}
		Editor editor = pref.edit();
		editor.putString(key, jsonArray.toString());
		System.out.println(jsonArray.toString());
		editor.commit();
	}
	
}
