package muict.bailarn.annotation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

import muict.bailarn.R;
import muict.bailarn.activity.MaterialViewer;
import android.os.Environment;

/*
 * This class will read/write files from device sdcard 
 * Folder Structure
 * Bailarn->Materials->Material Unique Name->PageNo
 */
public class FilesManager {
    // SDCard Path
    private final String MAT_PATH;
    private final String appName;

    private String currentPath = ""; //update every time user changes page
    
    // Audio
    private static final String AUDIO_FILE_PREFIX = "AUD_";
	private static final String AUDIO_FILE_SUFFIX = ".3gp"; //Related to AUDIO_OUTPUT_FORMAT in MainActivity
  
    //Image
    private static final String IMG_FILE_PREFIX = "IMG_";
	private static final String IMG_FILE_SUFFIX = ".jpg";
    
    
    public FilesManager(String appName){
    	this.appName = appName;
    	MAT_PATH = Environment.getExternalStorageDirectory().getAbsolutePath()+
        		"/" + appName + "/Materials/" + MaterialViewer.courseTitle + "/" + MaterialViewer.materialName ;
    }
    
    public void setCurrentPage(int page){
    	currentPath = MAT_PATH  + "/" + page;
    }
    
    public String getMatPath(){
    	return MAT_PATH;
    }
    
    public String getCurrentPath(){
    	return currentPath;
    }
    
    /**
     * Class to filter files which are having .3gp extension
     * */
    class AudioExtensionFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            return (name.endsWith(".3gp") );
        }
    }
    class TextExtensionFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            return (name.endsWith(".txt"));
        }
    }
    class ImgExtensionFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            return (name.endsWith(".jpg") || name.endsWith(".png") ); //allow reading .png file
        }
    }
 
    //================================== Audio ==============================
    /**
     * Function to read all 3gp files of a specified page of a particular material
     * and store the details in ArrayList
     * */
    public ArrayList<HashMap<String, String>> getPlayList(){
        File home = new File(currentPath);
        ArrayList<HashMap<String, String>> songsList = new ArrayList<HashMap<String, String>>();
        if (home.exists() && home.listFiles(new AudioExtensionFilter()).length > 0) {
            for (File file : home.listFiles(new AudioExtensionFilter())) {
                HashMap<String, String> song = new HashMap<String, String>();
                song.put("songTitle", file.getName().substring(0, (file.getName().length() - AUDIO_FILE_SUFFIX.length() )));
                song.put("songPath", file.getPath());
 
                // Adding each song to SongList
                songsList.add(song);
            }
        }
        // return songs list array
        return songsList;
    }
    
    public String getAudioFilename() {
		File file = new File(currentPath);
		if (!file.exists()) {
			file.mkdirs();
		}
		//return (file.getAbsolutePath() + "/" + System.currentTimeMillis() + AUDIO_OUTPUT_EXT);
		//int newSize = songsList.size() + 1;
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		return (file.getAbsolutePath() + "/" + AUDIO_FILE_PREFIX + timeStamp + AUDIO_FILE_SUFFIX);
	}
    
    //====================== Image ====================
    public ArrayList<HashMap<String, String>> getImgList(){
    	File home = new File(currentPath);
    	ArrayList<HashMap<String, String>> imgList = new ArrayList<HashMap<String, String>>();
        if (home.exists() && home.listFiles(new ImgExtensionFilter()).length > 0) {
            for (File file : home.listFiles(new ImgExtensionFilter())) {
                HashMap<String, String> song = new HashMap<String, String>();
                song.put("imgTitle", file.getName().substring(0, (file.getName().length() - IMG_FILE_SUFFIX.length() )));
                song.put("imgPath", file.getPath());
                imgList.add(song);
            }
        }
        return imgList;
    }
    public File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        //String imageFileName = IMG_FILE_PREFIX + timeStamp;
    	//int newSize = imgList.size() + 1;
    	String imageFileName = IMG_FILE_PREFIX + timeStamp + IMG_FILE_SUFFIX;
        File home = new File(currentPath);
		if (!home.exists()) {
			home.mkdirs();
		}
        File image = new File(home, imageFileName);
        /*File image = File.createTempFile(
            imageFileName,  		 prefix 
            IMG_FILE_SUFFIX,          suffix 
            home					 directory 
         );*/
        // Save a file: path for use with ACTION_VIEW intents
        //String mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }
    
    public void copyFile(File src, File dst) throws IOException {
    	FileUtils.copyFile(src, dst);
     }
    
    //================================ Text =============================
    public boolean writeTextFile(String text){
    	if(isExternalStorageWritable()){
    		try{
    			//String filename = currentPath + "/Note.txt";
    			File file = new File(currentPath);
    			if(!file.exists()) file.mkdir();
                FileOutputStream out = new FileOutputStream(new File(currentPath+"/Note.txt"));
                out.write(text.getBytes());
                out.close();  
             } catch (Exception e) { 
                e.printStackTrace();
             }
    		return true;
    	}
    	return false;
    }
    
    public String readTextFile(File file){
    	StringBuilder text = new StringBuilder();
		try {
		    BufferedReader br = new BufferedReader(new FileReader(file)); //we have only one text file per page folder
		    String line;
		    while ((line = br.readLine()) != null) {
		        text.append(line);
		        text.append('\n');
		    }
		}
		catch (IOException e) {
		    //You'll need to add proper error handling here
		}
		return text.toString();
    }
    
    public String getText(){
    	if(!isExternalStorageReadable() ){
    		return "Sorry. The storage is currently not readable";
    	}
    	else{
    		File home = new File(currentPath);
    		if (home.exists() && home.listFiles(new TextExtensionFilter()).length > 0) {
    			File files[] = home.listFiles(new TextExtensionFilter());
				//Read text from file
				StringBuilder text = new StringBuilder();
				try {
				    BufferedReader br = new BufferedReader(new FileReader(files[0])); //we have only one text file per page folder
				    String line;
				    while ((line = br.readLine()) != null) {
				        text.append(line);
				        text.append('\n');
				    }
				}
				catch (IOException e) {
				    //You'll need to add proper error handling here
				}
				return text.toString();
    		}
    		return ""; //no text file to read
    	}

    }
    
    
    //=============================================================
    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
            Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }
    
    //------------
    public void deleteOldFiles(){
		// Used to examplify deletion of files more than 1 month old
		// Note the L that tells the compiler to interpret the number as a Long
		final long MAXFILEAGE = 2678400000L * 3; // 1 month in milliseconds. I add *3 for 3 months (1 semester)
		//final long MAXFILEAGE = 1000*60; //1 min test
		
		// Get file handle to the directory. In this case the application files dir
		File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+appName);
		
		if(dir.exists()){
			// Optain list of files in the directory. 
			// listFiles() returns a list of File objects to each file found.
			List<File> files = (List<File>) FileUtils.listFiles(dir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
			
			if(files != null){
				// Loop through all files
				for (File f : files ) {

				   // Get the last modified date. Miliseconds since 1970
				   Long lastmodified = f.lastModified();

				   // Do stuff here to deal with the file.. 
				   // For instance delete files older than 1 month
				   if(lastmodified +MAXFILEAGE < System.currentTimeMillis()) {
				      f.delete();
				   }
				}
			}
		}
		
		
	}
}
