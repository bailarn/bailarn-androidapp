package muict.bailarn.annotation;

import java.io.File;

import muict.bailarn.R;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;

public class FullScreenImage extends Activity
{
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	   setContentView(R.layout.fullscreen_image);
	   Intent intent = getIntent();
	   String imgPath = (String) intent.getExtras().get("imgPath");
	   ImageView imageView = (ImageView)findViewById(R.id.imgDisplay);
	   
	   File imgFile = new File(imgPath);
	   if(imgFile.exists()){
	       Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
	       imageView.setImageBitmap(myBitmap);
        // imageView.setScaleType(ImageView.ScaleType.FIT_XY); //make pic fit to screen but disable our zoom function :(
  
	   }
	   
	}
 }
