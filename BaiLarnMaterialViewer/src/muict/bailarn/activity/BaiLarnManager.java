package muict.bailarn.activity;

import java.util.ArrayList;
import java.util.List;

import com.example.retrofitandroid.response.Course;
import com.example.retrofitandroid.response.Event;

import muict.bailarn.R;
import muict.bailarn.navigation.NavigationAdapter;
import muict.bailarn.navigation.NavigationItem;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.AdapterView;
import android.widget.ListView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

public class BaiLarnManager extends FragmentActivity{
    
    private SharedPreferences sharedPreferences;
    private String preferences;
    
////////////////////////////////////////////////////////
    
    private DrawerLayout drawerLayout;
	private ListView drawerList;
	private ActionBarDrawerToggle drawerToggle;

	// slide menu items
	private String[] navTitle;
	private TypedArray navIcon;

	private ArrayList<NavigationItem> navItem;
	private NavigationAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bailarn_manager);
 
        preferences = getString(R.string.preferences);
        sharedPreferences = getSharedPreferences(preferences,
				Context.MODE_PRIVATE);
        
        createNavigation();
        if (savedInstanceState == null) {
        	selectItem(10);
        }
	}
	
	private void createNavigation() {
		// mTitle = mDrawerTitle = getTitle();

		// load slide menu items
		navTitle = getResources().getStringArray(R.array.nav_array);

		// nav drawer icons from resources
		navIcon = getResources()
				.obtainTypedArray(R.array.nav_drawer_icons);

		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawerList = (ListView) findViewById(R.id.left_drawer);

		navItem = new ArrayList<NavigationItem>();

		// adding nav drawer items to array
		// Home
		navItem.add(new NavigationItem(navTitle[0], navIcon.getResourceId(0, -1)));
		// Calendar
		navItem.add(new NavigationItem(navTitle[1], navIcon.getResourceId(1, -1), true, "1"));
		// Wolfram
		navItem.add(new NavigationItem(navTitle[2], navIcon.getResourceId(2, -1), true, "20+"));
		// YouTube
		navItem.add(new NavigationItem(navTitle[3], navIcon.getResourceId(3, -1)));
		// Logout
		navItem.add(new NavigationItem(navTitle[4], navIcon.getResourceId(4, -1)));
		// Communities, Will add a counter here
		/*navItem.add(new NavDrawerItem(navTitle[3], navIcon.getResourceId(3, -1), true, "22"));
		// Pages
		navItem.add(new NavDrawerItem(navTitle[4], navIcon.getResourceId(4, -1)));
		// What's hot, We  will add a counter here
		navItem.add(new NavDrawerItem(navTitle[5], navIcon.getResourceId(5, -1), true, "50+"));*/
		

		// Recycle the typed array
		navIcon.recycle();

		// setting the nav drawer list adapter
		adapter = new NavigationAdapter(getApplicationContext(),
				navItem);
		drawerList.setAdapter(adapter);

		// enabling action bar app icon and behaving it as toggle button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		drawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
				R.drawable.ic_drawer, //nav menu toggle icon
				R.string.drawer_open, // nav drawer open - description for accessibility
				R.string.drawer_close // nav drawer close - description for accessibility
				){
					public void onDrawerClosed(View view) {
						//getActionBar().setTitle(mTitle);
						// calling onPrepareOptionsMenu() to show action bar icons
						invalidateOptionsMenu();
					}
		
					public void onDrawerOpened(View drawerView) {
						//getActionBar().setTitle("BaiLarn");
						// calling onPrepareOptionsMenu() to hide action bar icons
						invalidateOptionsMenu();
					}
				};
		drawerLayout.setDrawerListener(drawerToggle);
		drawerList.setOnItemClickListener(new DrawerItemClickListener());
	}
	
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        	selectItem(position);
            // update selected item and title, then close the drawer
        	drawerList.setItemChecked(position, true);
			drawerList.setSelection(position);
			drawerLayout.closeDrawer(drawerList);
        }
    }
	
	private void selectItem(int position) {
		Fragment fragment;
		if (position == 10) {
			fragment = new UserProfileActivity();
	        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
	        transaction.replace(R.id.content_frame, fragment)
	        		.commit();
    	}
		
		if (position == 0) {
	        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
	        	getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
	        }
    	}
    	else if (position == 1) {
    		fragment = new CalendarActivity();
	        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
	        transaction.replace(R.id.content_frame, fragment)
	        		.addToBackStack(null)
	        		.commit();
    	}
    	else if (position == 2) {
    		;
    	}
    	else if (position == 3) {
    		;
    	}
    	else if (position == 4) {
    		logout();
    	}
		
		
	}
	
	public void displayCourses(final String json, String courseTitle) {
		Fragment fragment = new CourseInfoActivity();
        Bundle args = new Bundle();
        args.putString("Course", json);
        fragment.setArguments(args);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(R.id.content_frame, fragment)
        			.addToBackStack(null)
        			.commit();
	}
    
	public void displayEvent(final String event) {
		Fragment fragment = new EventDetails();
		Bundle args = new Bundle();
		args.putString("event", event);
		fragment.setArguments(args);
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(R.id.content_frame, fragment)
				.addToBackStack(null)
				.commit();
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		if (drawerToggle.onOptionsItemSelected(item)) {
	        return true;
	    }
		return super.onOptionsItemSelected(item);
	}
	
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawerToggle.onConfigurationChanged(newConfig);
    }
    
    public void logout() {
		Editor prefsEditor = sharedPreferences.edit();
		prefsEditor.clear().commit();

		Intent myIntent = new Intent(BaiLarnManager.this,
				MainActivity.class);
		BaiLarnManager.this.startActivity(myIntent);
		finish(); // Go back to login page
	}

}
