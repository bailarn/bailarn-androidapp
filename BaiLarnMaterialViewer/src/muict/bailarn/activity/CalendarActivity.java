package muict.bailarn.activity;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.example.retrofitandroid.BaiLarnService;
import com.example.retrofitandroid.DateDeserializer;

import muict.bailarn.R;

import com.example.retrofitandroid.request.UserUsageRequest;
import com.example.retrofitandroid.response.Course;
import com.example.retrofitandroid.response.Event;
import com.example.retrofitandroid.response.Material;
import com.example.retrofitandroid.response.Token;
import com.example.retrofitandroid.response.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;

import android.annotation.SuppressLint;
import android.app.ActionBar.LayoutParams;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;
//import android.annotation.SuppressLint;
//import android.app.Activity;
//import android.app.ActionBar.LayoutParams;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.LayoutDirection;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class CalendarActivity extends Fragment{
	
	private Token userToken;
	private RestAdapter restAdapter;
	private BaiLarnService service;
	
	private String api_endpoint;
	private String client_id;
	private String client_secret;
	private String preferences;
	
	private SharedPreferences sharedPreferences;

	private User userProfile;
	private Event eventInfo;
	
	private View rootView;
	
	@Override

	//Initial the header and the view for the page
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		rootView = inflater.inflate(R.layout.calendar_list_item, container, false);
//		setContentView(R.layout.activity_course_info);

//		// Initialization
		//CalendarListView = (ListView) rootView.findViewById(R.id.materialListView);

		api_endpoint = getString(R.string.api_endpoint);
		client_id = getString(R.string.client_id);
		client_secret = getString(R.string.client_secret);
		preferences = getString(R.string.preferences);
		// OkHttp Cache Setup
		OkHttpClient okHttpClient = new OkHttpClient();
//		File cacheDir = getBaseContext().getCacheDir();
		File cacheDir = getActivity().getBaseContext().getCacheDir();
		Cache cache = null;
		try {
			cache = new Cache(cacheDir, 1024);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		okHttpClient.setCache(cache);
		// //////////////////
		// Rest Client Setup
		Gson gson = new GsonBuilder().registerTypeAdapter(Date.class,
				new DateDeserializer()).create();

		restAdapter = new RestAdapter.Builder()
				.setClient(new OkClient(okHttpClient))
				.setLogLevel(RestAdapter.LogLevel.FULL)
				.setConverter(new GsonConverter(gson))
				.setEndpoint(api_endpoint).build();
		service = restAdapter.create(BaiLarnService.class);
		// /////////////////
		// Shared Preferences Setup
		sharedPreferences = getActivity().getSharedPreferences(preferences,
				Context.MODE_PRIVATE);
		// //////////////
		// ////////////////////////////////////////////////////
		// ////////////////////////////////////////////////////
		// ////////////////////////////////////////////////////

		// Check for existing token
		if (sharedPreferences.contains("userToken")) {
			gson = new Gson();
			String json = sharedPreferences.getString("userToken", "");
			userToken = gson.fromJson(json, Token.class);
		}
		// Check for existing userProfile, then display it
		if (sharedPreferences.contains("userProfile")) {
			gson = new Gson();
			String json = sharedPreferences.getString("userProfile", "");
			userProfile = gson.fromJson(json, User.class);
		}
		// ////////////////////////////////////////////////////
		// ////////////////////////////////////////////////////
		// ////////////////////////////////////////////////////
		getEvents();
		return rootView;
	}
	
	//Extract event from where they are stored
	private void getEvents() {
		service.getEvents(userToken.getAccess_token(),
				new Callback<List<Event>>() {

					@Override
					public void failure(RetrofitError arg0) {
						
						 Toast.makeText(getActivity().getApplicationContext(),
						 "Event Retrieval Failed : "+ arg0.getMessage(),
						 Toast.LENGTH_SHORT) .show();
						 
					}

					@Override
					public void success(List<Event> events, Response arg1) {

						displayEvents(events);
						Log.e("Heyy",String.valueOf(events.size()));
						
						Editor prefsEditor = sharedPreferences.edit();
						prefsEditor.commit();
					}

				});
	}
	
	//Finally, show them to the world.
	
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public void displayEvents(final List<Event> events) {
		
		//Biggest
		LinearLayout AllLayout = (LinearLayout) rootView.findViewById(R.id.allEventLayout);
		LinearLayout.LayoutParams allLayoutParam = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

		TextView Head = new TextView(getActivity());
		Head.setGravity(Gravity.CENTER);
		Head.setText("Let's see what we've got here!");
		Head.setPadding(10,20,10,40);
		Head.setTextSize(20);
		AllLayout.addView(Head, allLayoutParam);
		
		Comparator<Event> comp = new Comparator<Event>() {
		public int compare(Event e1, Event e2)
		{
			int i = e1.getDueDate().compareTo(e2.getDueDate());
				if (i != 0) return i;

			i = e1.getEventType().compareTo(e2.getEventType());
				if (i != 0) return i;

			return 0;
		}};
		Collections.sort(events,comp);
		
		ListAdapter adapter = new ArrayAdapter<Event>(getActivity(),
				android.R.layout.simple_list_item_1, events);
		
		//Print each element using for-loop
		
		final int adapterCount = adapter.getCount();
		Date today = new Date();
		
		for (int i = 0; i < adapterCount; i++) {
			
		if(i!=0) 
			if(events.get(i).getDueDate().getYear() != events.get(i-1).getDueDate().getYear())
			{
				LinearLayout yearLayout = new LinearLayout(getActivity().getBaseContext());
				LinearLayout.LayoutParams yearLayoutParam = new LinearLayout.LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				yearLayoutParam.setMargins(20,10,20,10);
				
				TextView Year = new TextView(getActivity());
				Year.setGravity(Gravity.CENTER_VERTICAL);
				Year.setText(Integer.toString(events.get(i).getDueDate().getYear() + 1900));
				Year.setPadding(10,20,10,20);
				Year.setTextColor(Color.parseColor("#1abc9c"));
				Year.setTextSize(18);
				yearLayout.addView(Year, yearLayoutParam);
				AllLayout.addView(yearLayout, allLayoutParam);
			}
			
			//each line (= dayLayout) has two sub-layouts --> Date zone & event block zone
			LinearLayout dayLayout = new LinearLayout(getActivity().getBaseContext());
			LinearLayout.LayoutParams dayLayoutParam = new LinearLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			dayLayoutParam.setMargins(20,10,20,10);
			
			//First block, date zone
			LinearLayout dateLayout = new LinearLayout(getActivity().getBaseContext());
			LinearLayout.LayoutParams dateLayoutParam = new LinearLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			dateLayout.setGravity(Gravity.CENTER);
			dateLayout.setOrientation(LinearLayout.VERTICAL);
			
			//Inside dateLayout, it has 2 sub children				
				TextView eDay = new TextView(getActivity());
				TextView eMonth = new TextView(getActivity());
				eDay.setWidth(55);
				eDay.setHeight(40);
				eDay.setGravity(Gravity.CENTER);
				eDay.setTextColor(Color.parseColor("#1abc9c"));
				
				eMonth.setPadding(2,0,2,0);
				eMonth.setTextColor(Color.parseColor("#1abc9c"));
				
				//Check the date that it need To be printed or not
					Date cur = events.get(i).getDueDate();
					
					if(today.getDate() == cur.getDate() && today.getMonth() == cur.getMonth() && today.getYear() == cur.getYear())  eDay.setText("Today");			//<----- change to date=today.getDate blah blah
					else {
						
						eDay.setTextSize(22);
						Date prev;
						if(i!=0)  {
							prev = events.get(i-1).getDueDate();
							if(prev.getDate() != cur.getDate() || prev.getMonth() != cur.getMonth() || prev.getYear() != cur.getYear())
							{
								eDay.setText(Integer.toString(cur.getDate()));
								eMonth.setText(cur.toString().substring(4,7));
							}
							else eMonth.setText("  "); 
						}
						else {
							eDay.setText(Integer.toString(cur.getDate()));
							eMonth.setText(cur.toString().substring(4,7));
						}
					}
					dateLayout.addView(eDay,dateLayoutParam);
					dateLayout.addView(eMonth,dateLayoutParam);
					dayLayout.addView(dateLayout, dayLayoutParam);
			
			//End part of initializing date at the front
	//======================================================================================================================				
	//======================================================================================================================
			//Second block, event zone
			LinearLayout eLayout = new LinearLayout(getActivity().getBaseContext());
			eLayout.setGravity(20);
			eLayout.setPadding(10, 0, 10, 0);
			
			final View item = adapter.getView(i, null, null);

			final int position = i;
			
			if(events.get(i).getEventType().equals("assignment")) eLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.e_assignment_box));
			else if(events.get(i).getEventType().contains("activity")) eLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.e_activity_box));
			else if(events.get(i).getEventType().equals("exam")) eLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.e_quiz_exam_box));
			else eLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.grey_box));

	//--------------------------------------------------------------------------------------------------------------
			//declare for clicking an event
			eLayout.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Event selectedEvent = events.get(position);
					UserUsageRequest request = new UserUsageRequest(
							userProfile.getId(), selectedEvent.getId());
					service.accessEvent(userToken.getAccess_token(), request, new Callback<Response>() {
						@Override
							public void failure(RetrofitError arg0) {}
						@Override
							public void success(Response arg0, Response arg1) {}
					});
					
//					Intent intent = new Intent(getActivity().getApplicationContext(),
//							EventDetails.class);
//					intent.putExtra("event", selectedEvent);
//					startActivity(intent);
					Log.e("EVENT", selectedEvent.toString());
					
					BaiLarnManager bailarn = (BaiLarnManager)getActivity();
					Gson gson = new Gson();
					
					final String json = gson.toJson(selectedEvent);
					Log.e("EVENT json", json);
					bailarn.displayEvent(selectedEvent.toParseString());
				}
			});//end SetOnClickListener
	// ------------------------------------------------------------------------------------------------------------------------
			LinearLayout.LayoutParams layoutParam = new LinearLayout.LayoutParams(
														LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
			layoutParam.setMargins(10, 5, 20, 5);
			
			eLayout.addView(item,layoutParam);
			
			dayLayout.addView(eLayout,layoutParam);
			// ------------------------------------------------------------------------------------------------------------------------
			
		AllLayout.addView(dayLayout, allLayoutParam);
		}//End for loop
		
		TextView Foot = new TextView(getActivity());
		Foot.setGravity(Gravity.CENTER);
		Foot.setText("That's all you have. Hooray!");
		Foot.setPadding(10,40,10,20);
		Foot.setTextSize(20);
		AllLayout.addView(Foot, allLayoutParam);
	}
}