package muict.bailarn.activity;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

import com.example.retrofitandroid.BaiLarnService;
import com.example.retrofitandroid.DateDeserializer;

import muict.bailarn.R;

import com.example.retrofitandroid.response.Class;
import com.example.retrofitandroid.response.Material;
import com.example.retrofitandroid.response.Token;
import com.example.retrofitandroid.response.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ClassInfoActivity extends Activity {

	private TextView classInfoTextView;
	private ListView materialListView;

	private Token userToken;
	private RestAdapter restAdapter;
	private BaiLarnService service;

	private String api_endpoint;
	private String client_id;
	private String client_secret;
	private String preferences;

	private SharedPreferences sharedPreferences;

	private User userProfile;
	private Class classInfo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_class_info);
		// ////////////////////////////////////////////////////
		// ////////////////////////////////////////////////////
		// ////////////////////////////////////////////////////
		// Initialization
		classInfoTextView = (TextView) findViewById(R.id.classInfoTextView);
		materialListView = (ListView) findViewById(R.id.materialListView);

		api_endpoint = getString(R.string.api_endpoint);
		client_id = getString(R.string.client_id);
		client_secret = getString(R.string.client_secret);
		preferences = getString(R.string.preferences);
		// OkHttp Cache Setup
		OkHttpClient okHttpClient = new OkHttpClient();
		File cacheDir = getBaseContext().getCacheDir();
		Cache cache = null;
		try {
			cache = new Cache(cacheDir, 1024);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		okHttpClient.setCache(cache);
		// //////////////////
		// Rest Client Setup
		Gson gson = new GsonBuilder().registerTypeAdapter(Date.class,
				new DateDeserializer()).create();

		restAdapter = new RestAdapter.Builder()
				.setClient(new OkClient(okHttpClient))
				.setLogLevel(RestAdapter.LogLevel.FULL)
				.setConverter(new GsonConverter(gson))
				.setEndpoint(api_endpoint).build();
		service = restAdapter.create(BaiLarnService.class);
		// /////////////////
		// Shared Preferences Setup
		sharedPreferences = getSharedPreferences(preferences,
				Context.MODE_PRIVATE);
		// //////////////
		// ////////////////////////////////////////////////////
		// ////////////////////////////////////////////////////
		// ////////////////////////////////////////////////////

		// Check for existing token
		if (sharedPreferences.contains("userToken")) {

			gson = new Gson();
			String json = sharedPreferences.getString("userToken", "");
			userToken = gson.fromJson(json, Token.class);
		}
		// //////////////
		// ////////////////////////////////////////////////////
		// ////////////////////////////////////////////////////
		// ////////////////////////////////////////////////////

		String classJson = getIntent().getStringExtra("Class");
		gson = new Gson();
		classInfo = gson.fromJson(classJson, Class.class);
		displayClassInfo(classInfo);
		if (sharedPreferences.contains(classInfo.getId())) {
			gson = new Gson();
			String json = sharedPreferences.getString(classInfo.getId(), "");
			Type listType = new TypeToken<List<Material>>() {
			}.getType();
			List<Material> materials = new Gson().fromJson(json, listType);
			displayMaterials(materials);
		}
		getMaterials();

	}

	private void displayClassInfo(Class classInfo) {
		classInfoTextView.setText(classInfo.toString());
	}

	private void getMaterials() {
		service.getMaterialsFromClass(userToken.getAccess_token(),
				classInfo.getId(), new Callback<List<Material>>() {

					@Override
					public void failure(RetrofitError arg0) {
						Toast.makeText(
								getApplicationContext(),
								"Material Retrieval Failed" + arg0.getMessage(),
								Toast.LENGTH_LONG).show();
					}

					@Override
					public void success(List<Material> materials, Response arg1) {
						displayMaterials(materials);

						Editor prefsEditor = sharedPreferences.edit();
						Gson gson = new Gson();
						String json = gson.toJson(materials);
						prefsEditor.putString(classInfo.getId(), json);
						prefsEditor.commit();
					}

				});
	}

	public void displayMaterials(List<Material> materials) {
		LinearLayout layout = (LinearLayout) findViewById(R.id.materialLayout);
		layout.removeAllViews();
		ListAdapter adapter = new ArrayAdapter<Material>(this,
				android.R.layout.simple_list_item_1, materials); // Your
																	// adapter.
		final int adapterCount = adapter.getCount();
		for (int i = 0; i < adapterCount; i++) {
			final View item = adapter.getView(i, null, null);
			final int position = i;
			item.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(getApplicationContext(), MaterialViewer.class);
		             intent.putExtra("course", classInfo.getCourse());
		            // intent.putExtra("material", materials.get(position));
		             startActivity(intent);
				}
			});
			layout.addView(item);
		}
	}
}
