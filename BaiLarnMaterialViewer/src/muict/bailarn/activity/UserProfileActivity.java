package muict.bailarn.activity;

import muict.bailarn.R;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

import com.example.retrofitandroid.BaiLarnService;
import com.example.retrofitandroid.DateDeserializer;
import com.example.retrofitandroid.request.TokenRenewalRequest;
import com.example.retrofitandroid.response.Class;
import com.example.retrofitandroid.response.Course;
import com.example.retrofitandroid.response.Token;
import com.example.retrofitandroid.response.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;

import android.R.drawable;
import android.annotation.SuppressLint;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class UserProfileActivity extends Activity {

	private TextView userProfileTextView;
	private ListView todayClassesListView;
	private ListView coursesListView;

	private Token userToken;
	private RestAdapter restAdapter;
	private BaiLarnService service;

	private String api_endpoint;
	private String client_id;
	private String client_secret;
	private String preferences;

	private SharedPreferences sharedPreferences;

	private User userProfile;

	private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] mPlanetTitles;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_profile);
		// ////////////////////////////////////////////////////
		// ////////////////////////////////////////////////////
		// ////////////////////////////////////////////////////
		// Initialization
		// todayClassesListView = (ListView)
		// findViewById(R.id.todayClassesListView);
		// coursesListView = (ListView) findViewById(R.id.courseListView);
		// setListViewHeightBasedOnChildren(coursesListView);
		
		api_endpoint = getString(R.string.api_endpoint);
		client_id = getString(R.string.client_id);
		client_secret = getString(R.string.client_secret);
		preferences = getString(R.string.preferences);
		// OkHttp Cache Setup
		OkHttpClient okHttpClient = new OkHttpClient();
		File cacheDir = getBaseContext().getCacheDir();
		Cache cache = null;
		try {
			cache = new Cache(cacheDir, 1024);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		okHttpClient.setCache(cache);
		// //////////////////
		// Rest Client Setup
		Gson gson = new GsonBuilder().registerTypeAdapter(Date.class,
				new DateDeserializer()).create();

		restAdapter = new RestAdapter.Builder()
				.setClient(new OkClient(okHttpClient))
				.setLogLevel(RestAdapter.LogLevel.FULL)
				.setConverter(new GsonConverter(gson))
				.setEndpoint(api_endpoint).build();
		service = restAdapter.create(BaiLarnService.class);
		// /////////////////
		// Shared Preferences Setup
		sharedPreferences = getSharedPreferences(preferences,
				Context.MODE_PRIVATE);

		// Check for existing token
		if (sharedPreferences.contains("userToken")) {
			gson = new Gson();
			String json = sharedPreferences.getString("userToken", "");
			userToken = gson.fromJson(json, Token.class);
		}

		// Check for existing userProfile, then display it
		if (sharedPreferences.contains("userProfile")) {
			gson = new Gson();
			String json = sharedPreferences.getString("userProfile", "");
			userProfile = gson.fromJson(json, User.class);
			displayProfile(userProfile);
		}
		if (sharedPreferences.contains("TodayClasses")) {
			gson = new Gson();
			String json = sharedPreferences.getString("TodayClasses", "");
			Type listType = new TypeToken<List<Class>>() {
			}.getType();
			List<Class> todayClasses = new Gson().fromJson(json, listType);
			displayTodayClasses(todayClasses);
		}
		if (sharedPreferences.contains("Courses")) {
			gson = new Gson();
			String json = sharedPreferences.getString("Courses", "");
			Type listType = new TypeToken<List<Course>>() {
			}.getType();
			List<Course> courses = new Gson().fromJson(json, listType);
			displayCourses(courses);
		}
		// //////////////
		// ////////////////////////////////////////////////////
		// ////////////////////////////////////////////////////
		// ////////////////////////////////////////////////////

		// Update user profile for the latest data
		getUserProfile();
//		getTodayClasses();
		getCourses();

		// todayClassesListView.setOnItemClickListener(new OnItemClickListener()
		// {
		//
		// @Override
		// public void onItemClick(AdapterView<?> parent, View view,
		// int position, long id) {
		// // TODO Auto-generated method stub
		// Class selectedClass = (Class) todayClassesListView
		// .getItemAtPosition(position);
		// Intent intent = new Intent(UserProfileActivity.this,
		// ClassInfoActivity.class);
		// Gson gson = new Gson();
		// String json = gson.toJson(selectedClass);
		// intent.putExtra("Class", json);
		// startActivity(intent);
		// }
		//
		// });

		// coursesListView.setOnItemClickListener(new OnItemClickListener() {
		//
		// @Override
		// public void onItemClick(AdapterView<?> parent, View view,
		// int position, long id) {
		// // TODO Auto-generated method stub
		// Course selectedCourse = (Course) coursesListView
		// .getItemAtPosition(position);
		// Intent intent = new Intent(UserProfileActivity.this,
		// CourseInfoActivity.class);
		// Gson gson = new Gson();
		// String json = gson.toJson(selectedCourse);
		// intent.putExtra("Course", json);
		// startActivity(intent);
		// }
		//
		// });
		
		mTitle = mDrawerTitle = getTitle();
        mPlanetTitles = getResources().getStringArray(R.array.nav_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mPlanetTitles));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // enable ActionBar app icon to behave as action to toggle nav drawer
        

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
//        mDrawerToggle = new ActionBarDrawerToggle(
//                UserProfileActivity.this,                  /* host Activity */
//                mDrawerLayout,         /* DrawerLayout object */
//                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
//                R.string.drawer_open,  /* "open drawer" description for accessibility */
//                R.string.drawer_close  /* "close drawer" description for accessibility */
//                ) {
//            public void onDrawerClosed(View view) {
//                //getActionBar().setTitle(mTitle);
//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
//            }
//
//            public void onDrawerOpened(View drawerView) {
//                //getActionBar().setTitle(mDrawerTitle);
//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
//            }
//        };
        
        
        mDrawerLayout.setDrawerListener(mDrawerToggle);
//        getActionBar().setDisplayHomeAsUpEnabled(true);
//        getActionBar().setHomeButtonEnabled(true);

        if (savedInstanceState == null) {
            selectItem(0);
        }
		
	}
	
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {
    	if (position == 0) {
    		
    	}
    	else if (position == 2) {
    		logout();
    	}

        // update selected item and title, then close the drawer
        
        //setTitle(mPlanetTitles[position]);
        	mDrawerLayout.closeDrawer(mDrawerList);
        	mDrawerList.setItemChecked(position, true);
    }

	public void getUserProfile() {
		service.getUserProfile(userToken.getAccess_token(),
				new Callback<User>() {

					@Override
					public void failure(RetrofitError arg0) {
						try {
/*							Toast.makeText(getApplicationContext(),
									"User Profile Retrieval Failed",
									Toast.LENGTH_SHORT).show();*/
							renewToken();

						} catch (NullPointerException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					@Override
					public void success(User user, Response arg1) {
						userProfile = user;
						Editor prefsEditor = sharedPreferences.edit();
						Gson gson = new Gson();
						String userJson = gson.toJson(user);
						prefsEditor.putString("userProfile", userJson);
						prefsEditor.commit();
/*						Toast.makeText(getApplicationContext(),
								"User Profile Retrieval Success",
								Toast.LENGTH_SHORT).show();*/
						displayProfile(user);
					}

				});
	}

	public void renewToken() {
		try {
			String refreshToken = userToken.getRefresh_token();
			TokenRenewalRequest request = new TokenRenewalRequest(refreshToken,
					client_id, client_secret);
			service.renewToken(request, new Callback<Token>() {

				@Override
				public void failure(RetrofitError err) {
/*					Toast.makeText(
							getApplicationContext(),
							"Token Renewal Failed : " + err.getMessage()
									+ " Please login again", Toast.LENGTH_SHORT)
							.show();*/
					//logout(null);
				}

				@Override
				public void success(Token token, Response arg1) {
					userToken = token;
					saveToken(token);
/*					Toast.makeText(getApplicationContext(),
							"Token Renewal Success", Toast.LENGTH_SHORT).show();*/

					// Retrieve data again after token renewal
					getUserProfile();
//					getTodayClasses();
					getCourses();
				}

			});
		} catch (NullPointerException e) {
			Toast.makeText(getApplicationContext(),
					"Token not found, please login first", Toast.LENGTH_SHORT)
					.show();
		}
	}

	private void saveToken(Token token) {
		Editor prefsEditor = sharedPreferences.edit();
		Gson gson = new Gson();
		String tokenJson = gson.toJson(token);
		prefsEditor.putString("userToken", tokenJson);
		prefsEditor.commit();

	}

	//public void logout(View v) {
	public void logout() {
		Editor prefsEditor = sharedPreferences.edit();
		prefsEditor.clear().commit();

		Intent myIntent = new Intent(UserProfileActivity.this,
				MainActivity.class);
		UserProfileActivity.this.startActivity(myIntent);
		finish(); // Go back to login page
	}

	private void displayProfile(User userProfile) {
		// /////////// Display Profile //////////
		TextView userProfileTextView = (TextView) findViewById(R.id.userProfileTextView);
		try {
			String userProfileString = "Name: " + userProfile.getFirstname()
					+ " " + userProfile.getLastname() + "\nFaculty: "
					+ userProfile.getFaculty().getName() + "\nUniversity: "
					+ userProfile.getUniversity().getName();
			userProfileTextView.setText(userProfileString);
		} catch (NullPointerException e) {
			e.printStackTrace();
/*			Toast.makeText(getApplicationContext(),
					"No user profile found. Please login again",
					Toast.LENGTH_SHORT).show();*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		// ///////////////////////////////////////
	}

	public void getTodayClasses() {
		service.getTodayClasses(userToken.getAccess_token(),
				new Callback<List<Class>>() {

					@Override
					public void failure(RetrofitError arg0) {
						// RestError error = arg0
/*						Toast.makeText(
								getApplicationContext(),
								"Today Class Retrieval Fail"
										+ arg0.getMessage(), Toast.LENGTH_SHORT)
								.show();*/
						renewToken();
					}

					@Override
					public void success(List<Class> classes, Response arg1) {
/*						Toast.makeText(getApplicationContext(),
								"Today Class Retrieval Success",
								Toast.LENGTH_SHORT).show();*/
						displayTodayClasses(classes);

						Editor prefsEditor = sharedPreferences.edit();
						Gson gson = new Gson();
						String json = gson.toJson(classes);
						prefsEditor.putString("TodayClasses", json);
						prefsEditor.commit();
					}

				});
	}

	public void getCourses() {
		service.getCourses(userToken.getAccess_token(),
				new Callback<List<Course>>() {

					@Override
					public void failure(RetrofitError arg0) {
/*						Toast.makeText(getApplicationContext(),
								"Courses Retrieval Failed", Toast.LENGTH_SHORT)
								.show();*/
						renewToken();
					}

					@Override
					public void success(List<Course> courses, Response arg1) {
						// TODO Auto-generated method stub
/*						Toast.makeText(getApplicationContext(),
								"Courses Retrieval Success", Toast.LENGTH_SHORT)
								.show();*/
						displayCourses(courses);

						Editor prefsEditor = sharedPreferences.edit();
						Gson gson = new Gson();
						String json = gson.toJson(courses);
						prefsEditor.putString("Courses", json);
						prefsEditor.commit();
					}

				});
	}

	public void displayTodayClasses(final List<Class> todayClasses) {
		// ArrayAdapter<Class> listAdapter = new ArrayAdapter<Class>(this,
		// android.R.layout.simple_list_item_1, todayClasses);
		// todayClassesListView.setAdapter(listAdapter);

		LinearLayout layout = (LinearLayout) findViewById(R.id.todayClassesLayout);
		layout.removeAllViews();
		ListAdapter adapter = new ArrayAdapter<Class>(this,
				android.R.layout.simple_list_item_1, todayClasses); // Your
																	// adapter.
		final int adapterCount = adapter.getCount();
		for (int i = 0; i < adapterCount; i++) {
			final View item = adapter.getView(i, null, null);
			final int position = i;
			item.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Class selectedClass = todayClasses.get(position);
					Intent intent = new Intent(UserProfileActivity.this,
							ClassInfoActivity.class);
					Gson gson = new Gson();
					String json = gson.toJson(selectedClass);
					intent.putExtra("Class", json);
					startActivity(intent);
				}
			});
			layout.addView(item);
		}
	}

	@SuppressLint("NewApi") public void displayCourses(final List<Course> courses) {
		// ArrayAdapter<Course> listAdapter = new ArrayAdapter<Course>(this,
		// android.R.layout.simple_list_item_1, courses);
		// coursesListView.setAdapter(listAdapter);

		LinearLayout layout = (LinearLayout) findViewById(R.id.coursesLayout);
		layout.removeAllViews();
		ListAdapter adapter = new ArrayAdapter<Course>(this,
				android.R.layout.simple_list_item_1, courses); // Your adapter.
		final int adapterCount = adapter.getCount();
		for (int i = 0; i < adapterCount; i++) {
			final View item = adapter.getView(i, null, null);
			final int position = i;
			LinearLayout courselayout = new LinearLayout(getBaseContext());
			courselayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.grey_box));
			ImageView cIcon = new ImageView(this);
	        cIcon.setBackgroundResource(R.drawable.ic_greenbook);
	        courselayout.setGravity(Gravity.CENTER_VERTICAL);
			courselayout.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Course selectedCourse = courses.get(position);
//					Intent intent = new Intent(UserProfileActivity.this,
//							CourseInfoActivity.class);
					Gson gson = new Gson();
					String json = gson.toJson(selectedCourse);
//					intent.putExtra("Course", json);
//					startActivity(intent);
					
					Fragment fragment = new CourseInfoActivity();
			        Bundle args = new Bundle();
			        args.putString("Course", json);
			        fragment.setArguments(args);

			        FragmentManager fragmentManager = getFragmentManager();
			        fragmentManager.beginTransaction()
			        			.replace(R.id.content_frame, fragment)
			        			.addToBackStack(null)
			        			.commit();

			        // update selected item and title, then close the drawer
//			        mDrawerList.setItemChecked(position, true);
//			        setTitle(mPlanetTitles[position]);
			        mDrawerLayout.closeDrawer(mDrawerList);
					
				}
			});
			// ------------------------------------------------------------------------------------------------------------------------
			LinearLayout.LayoutParams layoutParam = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
			layoutParam.setMargins(0, 0, 0, 50);
			courselayout.setPadding(15, 15, 15, 15);
			courselayout.addView(cIcon);
	        courselayout.addView(item);
			layout.addView(courselayout,layoutParam);
			// ------------------------------------------------------------------------------------------------------------------------
		}
	}

}
