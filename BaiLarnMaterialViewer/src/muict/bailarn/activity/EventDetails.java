package muict.bailarn.activity;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import com.example.retrofitandroid.BaiLarnService;
import com.example.retrofitandroid.DateDeserializer;

import muict.bailarn.R;

import com.example.retrofitandroid.response.Event;
import com.example.retrofitandroid.response.Token;
import com.example.retrofitandroid.response.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;

import android.annotation.SuppressLint;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class EventDetails extends Fragment{
	
	private Token userToken;
	private RestAdapter restAdapter;
	private BaiLarnService service;
	
	private String api_endpoint;
	private String client_id;
	private String client_secret;
	private String preferences;
	
	private SharedPreferences sharedPreferences;

	private User userProfile;
	private Event eventInfo;
	
	private View rootView;

	public static String DateHead = "Feb 13";
	public static String Type = "haha";
	public static String Title = "NSC Deadline";
	public static String Description = "Description : \n\t";
	public static String Start = "From : ";
	public static String End = "Until : ";
	public static String AnnouncedDate = "Announced on : ";
	public static String UpdatedDate = "Updated on : ";
	public static String Holder = "By : ";
	
	@SuppressWarnings("deprecation")
	@Override

	//Initial the header and the view for the page
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		rootView = inflater.inflate(R.layout.event_detail, container, false);
//		setContentView(R.layout.activity_course_info);

		DateHead = "Feb 13";
		Type = "haha";
		Title = "NSC Deadline";
		Description = "Description : \n\t";
		Start = "From : ";
		End = "Until : ";
		AnnouncedDate = "Announced on : ";
		UpdatedDate = "Updated on : ";
		Holder = "By : ";
		
//		// Initialization
		//CalendarListView = (ListView) rootView.findViewById(R.id.materialListView);

		api_endpoint = getString(R.string.api_endpoint);
		client_id = getString(R.string.client_id);
		client_secret = getString(R.string.client_secret);
		preferences = getString(R.string.preferences);
		// OkHttp Cache Setup
		OkHttpClient okHttpClient = new OkHttpClient();
//		File cacheDir = getBaseContext().getCacheDir();
		File cacheDir = getActivity().getBaseContext().getCacheDir();
		Cache cache = null;
		try {
			cache = new Cache(cacheDir, 1024);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		okHttpClient.setCache(cache);
		// //////////////////
		// Rest Client Setup
		Gson gson = new GsonBuilder().registerTypeAdapter(Date.class,
				new DateDeserializer()).create();

		restAdapter = new RestAdapter.Builder()
				.setClient(new OkClient(okHttpClient))
				.setLogLevel(RestAdapter.LogLevel.FULL)
				.setConverter(new GsonConverter(gson))
				.setEndpoint(api_endpoint).build();
		service = restAdapter.create(BaiLarnService.class);
		// /////////////////
		// Shared Preferences Setup
		sharedPreferences = getActivity().getSharedPreferences(preferences,
				Context.MODE_PRIVATE);
		// //////////////
		// ////////////////////////////////////////////////////
		// ////////////////////////////////////////////////////
		// ////////////////////////////////////////////////////

		// Check for existing token
		if (sharedPreferences.contains("userToken")) {
			gson = new Gson();
			String json = sharedPreferences.getString("userToken", "");
			userToken = gson.fromJson(json, Token.class);
		}
		// Check for existing userProfile, then display it
		if (sharedPreferences.contains("userProfile")) {
			gson = new Gson();
			String json = sharedPreferences.getString("userProfile", "");
			userProfile = gson.fromJson(json, User.class);
		}
		// ////////////////////////////////////////////////////
		// ////////////////////////////////////////////////////
		// ////////////////////////////////////////////////////
		
		String[] event = getArguments().getString("event").split("//");
		// dateOnTop - type - name - description - datetime_start - datetime_end - createdAt
		// -  updatedAt - announcer
		
		DateHead = event[0];
		Type = event[1];
		Title = event[2];
		Description += event[3];
		Start += event[4].substring(0,19);
		End += event[5].substring(0,19);
		AnnouncedDate += event[6].substring(0,19);
		UpdatedDate += event[7].substring(0,19);
		Holder += event[8];
		
		RelativeLayout eLayout = (RelativeLayout) rootView.findViewById(R.id.circle);
		GradientDrawable sd = (GradientDrawable) eLayout.getBackground();
	    
		
		if(Type.equals("assignment")) sd.setColor(Color.parseColor("#7FFF7537"));
		else if(Type.contains("activity")) sd.setColor(Color.parseColor("#7F3399FF"));
		else if(Type.equals("exam")) sd.setColor(Color.parseColor("#7F16A765"));

		displayEvents();
		return rootView;
	}
	
	//Finally, show them to the world.
	
//	@SuppressWarnings("deprecation")
//	@SuppressLint("NewApi")
//	public void displayEvents(final List<Event> events) {
//		
//		//Biggest
//		TextView date = (TextView) rootView.findViewById(R.id.eventDateTextView);
//		
//		date.setText(eventDate);
//		
//	}	
	
	public void displayEvents() {

		TextView dateHead = (TextView) rootView.findViewById(R.id.eventDateTextView);
		TextView title = (TextView) rootView.findViewById(R.id.eventTitleTextView);
		TextView des = (TextView) rootView.findViewById(R.id.EventDescription);
		TextView Edate = (TextView) rootView.findViewById(R.id.EventEnd);
		TextView Adate = (TextView) rootView.findViewById(R.id.EventAnnouncedDate);
		TextView hold = (TextView) rootView.findViewById(R.id.EventHolder);
		
		dateHead.setText(DateHead);
		title.setText(Title);
		des.setText(Description);
		
		if(Start.equals(End)) Edate.setText(End);
		else Edate.setText(Start + "\n\n" + End);
		
		if(AnnouncedDate.equals(UpdatedDate)) Adate.setText(AnnouncedDate);
		else Adate.setText(AnnouncedDate + "\n\n" + UpdatedDate);
		hold.setText(Holder);
		
	}
}