package muict.bailarn.activity;

import muict.bailarn.R;
import muict.bailarn.activity.MaterialViewer.DownloadFileFromURL;

import java.io.File;
import java.io.IOException;

import com.example.retrofitandroid.BaiLarnService;
import com.example.retrofitandroid.request.TokenRenewalRequest;
import com.example.retrofitandroid.request.UserLoginRequest;
import com.example.retrofitandroid.response.Token;
import com.google.gson.Gson;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends Activity {
	private TextView responseTV;
	private EditText usernameEditText;
	private EditText passwordEditText;

	private Token userToken;
	private RestAdapter restAdapter;
	private BaiLarnService service;

	private String api_endpoint;
	private String client_id;
	private String client_secret;
	private String preferences;

	private SharedPreferences sharedPreferences;

	public static boolean isNetworkAvailable(Context context) 
	{
	    return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		if(!isNetworkAvailable(getApplicationContext())){//check internet
			Toast.makeText(getApplicationContext(), "Please connect to Internet",
					Toast.LENGTH_LONG).show();
		}
		else{
			usernameEditText = (EditText) findViewById(R.id.usernameEditText);
			passwordEditText = (EditText) findViewById(R.id.passwordEditText);

			api_endpoint = getString(R.string.api_endpoint);
			client_id = getString(R.string.client_id);
			client_secret = getString(R.string.client_secret);
			preferences = getString(R.string.preferences);

			//OkHttp Cache Setup
			OkHttpClient okHttpClient = new OkHttpClient();
			File cacheDir = getBaseContext().getCacheDir();
			Cache cache = null;
			try {
				cache = new Cache(cacheDir, 1024);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			okHttpClient.setCache(cache);
			////////////////////
			
			//Rest Client Setup
			restAdapter = new RestAdapter.Builder()
					.setClient(new OkClient(okHttpClient))
					.setLogLevel(RestAdapter.LogLevel.FULL)
					.setEndpoint(api_endpoint).build();
			service = restAdapter.create(BaiLarnService.class);
			///////////////////
			
			//Shared Preferences Setup
			sharedPreferences = getSharedPreferences(preferences,
					Context.MODE_PRIVATE);
			////////////////
		}		
		
	}

	public void login(View view) {
		String username = usernameEditText.getText().toString();
		String password = passwordEditText.getText().toString();
		UserLoginRequest credential = new UserLoginRequest(username, password,
				client_id, client_secret);
		service.login(credential, new Callback<Token>() {

			@Override
			public void failure(RetrofitError arg0) {
				if(!isNetworkAvailable(getApplicationContext())){
					Toast.makeText(getApplicationContext(), "No Internet connection",
							Toast.LENGTH_LONG).show();
				}
				else{
					Toast.makeText(getApplicationContext(), "Fail",
							Toast.LENGTH_LONG).show();
				}
			}

			@Override
			public void success(Token token, Response arg1) {
				userToken = token;
				saveToken(token);
				finish();
			}
		});
	}

	public void renewToken() {
		try {
			String refreshToken = userToken.getRefresh_token();
			TokenRenewalRequest request = new TokenRenewalRequest(refreshToken,
					client_id, client_secret);
			service.renewToken(request, new Callback<Token>() {

				@Override
				public void failure(RetrofitError err) {
					Toast.makeText(getApplicationContext(),
							"Token Renewal Failed : " + err.getMessage(),
							Toast.LENGTH_SHORT).show();
				}

				@Override
				public void success(Token token, Response arg1) {
					userToken = token;
					saveToken(token);
/*					Toast.makeText(getApplicationContext(),
							"Token Renewal Success", Toast.LENGTH_LONG).show();*/
				}

			});
		} catch (NullPointerException e) {
			Toast.makeText(getApplicationContext(),
					"Token not found, please login first", Toast.LENGTH_SHORT)
					.show();
		}
	}
	
	private void saveToken(Token token)
	{
		Editor prefsEditor = sharedPreferences.edit();
		Gson gson = new Gson();
		String tokenJson = gson.toJson(token);
		prefsEditor.putString("userToken", tokenJson);
		prefsEditor.commit();
	
	}
}
