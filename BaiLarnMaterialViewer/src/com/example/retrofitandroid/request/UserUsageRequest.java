package com.example.retrofitandroid.request;

public class UserUsageRequest implements UsageRequest{
	public static final int TAKE_NOTE = 1;
	public static  final int TAKE_PHOTO = 2;
	public static final int RECORD_VOICE = 3;
	
	private String user;
	private String material;
	private int endpoint;
	
	public UserUsageRequest(String user, String material){
		this.user = user;
		this.material = material;
		endpoint = 0;
	}
	
	public void setEndpoint(int endpoint){
		this.endpoint = endpoint;
	}
	
	public int getEndpoint(){
		return endpoint;
	}
}
