package com.example.retrofitandroid.response;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Class {
	private String id;
	private Course course;
	private String location;
	private int section;
	private String status;
	private boolean lecture_flag;
	private boolean quiz_flag;
	private boolean lab_flag;
	private Date datetime_start;
	private Date datetime_end;
	
	
	public String getId() {
		return id;
	}

	public Date getDatetime_end() {
		return datetime_end;
	}

	public Date getDatetime_start() {
		return datetime_start;
	}

	public void setDatetime_start(Date datetime_start) {
		this.datetime_start = datetime_start;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getSection() {
		return section;
	}

	public void setSection(int section) {
		this.section = section;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isLecture_flag() {
		return lecture_flag;
	}

	public void setLecture_flag(boolean lecture_flag) {
		this.lecture_flag = lecture_flag;
	}

	public boolean isQuiz_flag() {
		return quiz_flag;
	}

	public void setQuiz_flag(boolean quiz_flag) {
		this.quiz_flag = quiz_flag;
	}

	public boolean isLab_flag() {
		return lab_flag;
	}

	public void setLab_flag(boolean lab_flag) {
		this.lab_flag = lab_flag;
	}
	
	public String getStartTime(){
		SimpleDateFormat printFormat = new SimpleDateFormat("HH:mm");
		return printFormat.format(datetime_start);
	}
	
	public String getEndTime(){
		SimpleDateFormat printFormat = new SimpleDateFormat("HH:mm");
		return printFormat.format(datetime_end);
	}
	
	public String toString(){
		return course.getName() + " " 
				+ location + " "
				+ getStartTime() + " - "
				+ getEndTime();
				
	}

}
