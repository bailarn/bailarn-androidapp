package com.example.retrofitandroid.response;

public class University {
	private String name;
	private String abbr_name;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAbbr_name() {
		return abbr_name;
	}
	public void setAbbr_name(String abbr_name) {
		this.abbr_name = abbr_name;
	}
	
	
}
