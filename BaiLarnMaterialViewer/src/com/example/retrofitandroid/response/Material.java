package com.example.retrofitandroid.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Material implements Serializable {
	
	//private Course course;
	private String name;
	private int version;
	//private User upload_by;
	//private String file_path;
	
//	public Class get_class() {
//		return _class;
//	}
//	public Course getCourse() {
//		return course;
//	}
	private String id;
	
	public String getId(){
		return id;
	}
	public String getName() {
		return name;
	}
	
	public int getVersion(){
		return version;
	}

//	public User getUpload_by() {
//		return upload_by;
//	}
//	public String getFile_path() {
//		return file_path;
//	}
	public String toString(){
		return name;
	}
	
}
