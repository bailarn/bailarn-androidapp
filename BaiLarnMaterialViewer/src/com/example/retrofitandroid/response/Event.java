package com.example.retrofitandroid.response;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Event implements Serializable {
	private String id;
	private String name;		//e.g. Homework 4
	private String type;	//e.g. Assignment, Presentation, Examination, Activities
	private String description;	//e.g. Submission, specification
	private Date createdAt;
	private Date updatedAt;
	private Date datetime_end;
	private Date datetime_start;
	private String announcer;

	@SuppressWarnings("deprecation")
	public Event(String i, String n, String t, int d, int m, int y){//,Date due) {
		id = i;
		name = n;
		type = t;
		datetime_end = new Date();
		datetime_end.setDate(d);
		datetime_end.setMonth(m-1);
		datetime_end.setYear(y-1900);
	}
	
	// private User teach_by;
	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEventType() {
		return type;
	}
	
	public String getAnnouncer() {
		return announcer;
	}

	public void setEventType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setAnnouncer(String ann) {
		this.announcer = ann;
	}

	public String toParseString() {
		return datetime_end.toString().substring(4,10) + "//" + type + "//" + name + "//" 
	           + description + "//"  + datetime_start + "//" + datetime_end + "//" 
			   + createdAt + "//" + updatedAt + "//" + announcer;
	}
	
	public String toString() {
		return name;
	}
	
	@SuppressWarnings("deprecation")
	public void setAnnouncedDate(int year,int month,int date,int hour, int minute,int second) {
		createdAt = new Date(year,month,date,hour,minute,second);
	}
	
	public Date getAnnouncedDate() {
		return createdAt;
	}
	
	@SuppressWarnings("deprecation")
	public void setStartDate(int year,int month,int date,int hour, int minute,int second) {
		datetime_start = new Date(year,month,date,hour,minute,second);
	}
	
	public Date getStartDate() {
		return datetime_start;
	}
	
	@SuppressWarnings("deprecation")
	public void setUpdateDate(int year,int month,int date,int hour, int minute,int second) {
		updatedAt = new Date(year,month,date,hour,minute,second);
	}
	
	public Date getUpdateDate() {
		return updatedAt;
	}
	
	@SuppressWarnings("deprecation")
	public void setDueDate(int year,int month,int date,int hour, int minute,int second) {
		datetime_end = new Date(year,month,date,hour,minute,second);
	}
	
	public Date getDueDate() {
		return datetime_end;
	}
}