package com.example.annotation2;

//Audio Player reference: http://www.androidhive.info/2012/03/android-building-audio-player-tutorial/
//Audio Recorder reference: http://androidcodeexamples.blogspot.com/2012/06/voice-recording-in-android.html

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SimpleAdapter;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	//Properties
	public static String materialName = "Lecture1_Introduction to MM";
	private int currentPage = 1; //default
	private int LAST_PAGE = 10; //let's say
	private TextView txtPageNumber;
	//Helper Class
	private FilesManager filesManager;	
	//Controllers
	private SeekBar audioProgressBar;
	private Button btnRecorder;
	private Button btnPlayer;
	private Button btnPlaylist;
	private EditText textbox;
	private Button btnSaveText;
	private Button btnNextPage;
	private Button btnPreviousPage;
	private ListView playlist;
	private Button btnTakePhoto;
	private ImgHorizontalLayout gallery;
	//Image
	static final int ACTION_TAKE_PHOTO = 100; //action code
	public ArrayList<HashMap<String, String>> imgList = new ArrayList<HashMap<String, String>>();
	
	//To-do imgList
	//Audio
	private int AUDIO_OUTPUT_FORMAT = MediaRecorder.OutputFormat.THREE_GPP;
	private boolean isRecording = false;//audio recorder status
	private MediaRecorder recorder;
	public ArrayList<HashMap<String, String>> songsList = new ArrayList<HashMap<String, String>>(); //audio list of a current page
	private  MediaPlayer mp;
	private Handler mHandler = new Handler();;
    private Utilities utils;

	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        //init
        mp = new MediaPlayer();
        filesManager = new FilesManager();
        audioProgressBar = (SeekBar) findViewById(R.id.seekBar1);
        utils = new Utilities();
        
        btnRecorder = (Button)findViewById(R.id.btnRecorder);
        btnPlayer = (Button) findViewById(R.id.btnPlayer);
        btnPlaylist = (Button) findViewById(R.id.btnPlaylist);
        textbox = (EditText)findViewById(R.id.textbox);
        btnSaveText = (Button) findViewById(R.id.btnSaveText);
        btnNextPage = (Button) findViewById(R.id.btnNextPage);
        btnPreviousPage = (Button) findViewById(R.id.btnPreviousPage);
        txtPageNumber = (TextView)findViewById(R.id.txtPageNumber);
        playlist = (ListView)findViewById(R.id.playList);
        txtPageNumber.setText(currentPage+"");
        btnTakePhoto = (Button)findViewById(R.id.btnTakePhoto);
        gallery = (ImgHorizontalLayout)findViewById(R.id.gallery);
        
    	setHandlers();
    	refresh();
    	// Toast.makeText(MainActivity.this, Environment.getExternalStorageDirectory().getPath()+ "/Bailarn/Materials/"+materialName+"/"+currentPage,
		//			Toast.LENGTH_LONG).show();
    	
		
    }
    
    //========== Page Handler ===========
    
    private void setHandlers() {
		btnRecorder.setOnClickListener(handleRecordClick);
		btnPlayer.setOnClickListener(handlePlayerClick);
		btnPlaylist.setOnClickListener(loadPlaylist);
		btnNextPage.setOnClickListener(new View.OnClickListener() {	 
            @Override
            public void onClick(View arg0) {
                goNextPage();
            }
        });
		btnPreviousPage.setOnClickListener(new View.OnClickListener() {	 
            @Override
            public void onClick(View arg0) {
                goPreviousPage();
            }
        });
		
		btnSaveText.setOnClickListener(saveText);
		btnTakePhoto.setOnClickListener(takePhoto);
		audioProgressBar.setOnSeekBarChangeListener(seekBarListener);
	}
    
    private void enableButton(int id, boolean isEnable) {
		((Button) findViewById(id)).setEnabled(isEnable);
	}
    
    //reload resources for a new page
    private void refresh(){
    	closePlaylist();	
    	filesManager.setCurrentPage(currentPage); //always call this before getting resources
    	songsList = filesManager.getPlayList(); //audio
    	loadGallery();
    	textbox.setText(filesManager.getText()); //text
    }
    
    //test when user moves to next page
    private void goPreviousPage(){
    	if(currentPage > 1){
    		currentPage--;
        	txtPageNumber.setText(currentPage+"");
        	refresh();
    	}	
    }
    private void goNextPage(){
    	if(currentPage < LAST_PAGE){
        	currentPage++;
        	txtPageNumber.setText(currentPage+"");
        	refresh();
    	}
    }
    
    //===================== Annotation
	
    private View.OnClickListener saveText = new View.OnClickListener() {
		@Override
		public void onClick(View view) {
			String theText = textbox.getText().toString();
			if(filesManager.writeTextFile(theText) ){
				Toast.makeText(MainActivity.this, "Note Saved",
						Toast.LENGTH_SHORT).show();
			}
			else{
				Toast.makeText(MainActivity.this, "Failed to save note",
						Toast.LENGTH_SHORT).show();
			}
		}
	};
	
	// ========== Image Zone =======
		private View.OnClickListener takePhoto = new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(!filesManager.isExternalStorageWritable()){
					Toast.makeText(MainActivity.this, "External storage is not available for write",
							Toast.LENGTH_SHORT).show();
				}
				else{
					//******** Camera Intent Start ***************//* 
					 Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					    // Ensure that there's a camera activity to handle the intent
					    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
					        // Create the File where the photo should go
					        File photoFile = null;
					        try {
					            photoFile = filesManager.createImageFile(); //to save photo file
					        } catch (IOException ex) {
					            // Error occurred while creating the File
					        	Log.v(getString(R.string.app_name), "ERROR in creating image file");
					        }
					        // Continue only if the File was successfully created
					        if (photoFile != null) {
					            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
					                    Uri.fromFile(photoFile));
					            startActivityForResult(takePictureIntent, ACTION_TAKE_PHOTO);
					        }
					    }
				}
			}
		};
		
		@Override
		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		    if (requestCode == ACTION_TAKE_PHOTO && resultCode == RESULT_OK) {
		    	//reload gallery
		    	loadGallery();
		        /*Bundle extras = data.getExtras();
		        Bitmap imageBitmap = (Bitmap) extras.get("data");
		        imageView.setImageBitmap(imageBitmap);*/
		    }
		}
		
		private void loadGallery(){
			gallery.clear();
			imgList = filesManager.getImgList();
			for(int i=0; i< imgList.size(); i++){
				gallery.add(imgList.get(i).get("imgPath"));
			}
		}
		
	
	/*
	 * 	=======================	Audio Zone =================
	 */

	private View.OnClickListener handleRecordClick = new View.OnClickListener() {
		@Override
		public void onClick(View view) {
			if (!isRecording) {
				Toast.makeText(MainActivity.this, "Start Recording",
						Toast.LENGTH_SHORT).show();
				startRecording();							
			} else {
				Toast.makeText(MainActivity.this, "Stop Recording",
						Toast.LENGTH_SHORT).show();
				stopRecording();
			}
			enableButton(R.id.btnPlayer, !isRecording);
			//update songsList
			songsList = filesManager.getPlayList();
		}
	};
    
    private View.OnClickListener handlePlayerClick = new View.OnClickListener() {
		@Override
		public void onClick(View v) {		
			// check for already playing
            if(mp.isPlaying()){
                if(mp!=null){
                    mp.pause();
                    // Changing button image to play button
                    //btnPlay.setImageResource(R.drawable.btn_play);
                    btnPlayer.setText(R.string.play_player);
                    Toast.makeText(MainActivity.this, R.string.pause_player,
    						Toast.LENGTH_SHORT).show();
                }
            }else{
                // Resume song
                if(mp!=null){
                    mp.start();
                    // Changing button image to pause button
                    //btnPlay.setImageResource(R.drawable.btn_pause);
                    btnPlayer.setText(R.string.pause_player);
                    Toast.makeText(MainActivity.this, R.string.play_player,
    						Toast.LENGTH_SHORT).show();
                }
            }
		}
	};
	
	//================= Playlist ================

     //Receiving song index from playlist view and play the song
	private View.OnClickListener loadPlaylist = new View.OnClickListener() {
		@Override
		public void onClick(View view) {
			if(playlist.getCount() > 0){
				closePlaylist();            
			}
			else{
				openPlaylist();				
			}
		}
	};  
	private void closePlaylist(){
		 ArrayAdapter<Object> la = new ArrayAdapter<Object>(getApplicationContext(), android.R.layout.simple_list_item_1);
         playlist.setAdapter(la);
         playlist.invalidateViews();
	}
	private void openPlaylist(){
		ListAdapter adapter = new SimpleAdapter(getApplicationContext(), songsList, R.layout.playlist_item, 
		          new String[] { "songTitle" },
		                new int[] { R.id.songTitle});

		playlist.setAdapter(adapter);
		playlist.setOnItemClickListener(new OnItemClickListener() {   
          @Override
          public void onItemClick(AdapterView<?> parent, View view,
                  int position, long id) {
              int songIndex = position;
              playSong(songIndex);
          }
      });
	}
	//================= Audio Player ==============
    @Override
    public void onDestroy(){
    super.onDestroy();
       mp.release();
    }
    /**
     * Function to play a song
     * @param songIndex - index of song
     * */
    public void  playSong(int songIndex){
        try {       	
            //songsList = filesManager.getPlayList(currentPage); // Getting all latest songs list
            mp.reset();
            mp.setDataSource(songsList.get(songIndex).get("songPath"));
            mp.prepare();
            mp.start();
            songsList.get(songIndex).get("songTitle");
 
            // Changing Button Image to pause image
            //btnPlay.setImageResource(R.drawable.btn_pause);
            btnPlayer.setText(R.string.pause_player);
            
            // set Progress bar values
            audioProgressBar.setProgress(0);
            audioProgressBar.setMax(100);
 
            // Updating progress bar
            updateProgressBar();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    //*** SeekBar Code **   
    private SeekBar.OnSeekBarChangeListener seekBarListener = new SeekBar.OnSeekBarChangeListener() {		 
	     //When user stops moving the progress 
		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			mHandler.removeCallbacks(mUpdateTimeTask);
	        int totalDuration = mp.getDuration();
	        int currentPosition = utils.progressToTimer(seekBar.getProgress(), totalDuration); 
	        // forward or backward to certain seconds
	        mp.seekTo(currentPosition); 
	        // update timer progress again
	        updateProgressBar();		
		}
		//When user starts moving the progress handler
		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
			// remove message Handler from updating progress bar
	        mHandler.removeCallbacks(mUpdateTimeTask);
		}
		
		@Override
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			// TODO Auto-generated method stub
			
		}
	};
    //update timer on seekbar
    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);
    } 
    //background runnable thread
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            long totalDuration = mp.getDuration();
            long currentDuration = mp.getCurrentPosition();

            // Displaying Total Duration time
            //songTotalDurationLabel.setText(""+utils.milliSecondsToTimer(totalDuration));
            // Displaying time completed playing
            //songCurrentDurationLabel.setText(""+utils.milliSecondsToTimer(currentDuration));

            // Updating progress bar
            int progress = (int)(utils.getProgressPercentage(currentDuration, totalDuration));
            //Log.d("Progress", ""+progress);
            audioProgressBar.setProgress(progress);

            // Running this thread after 100 milliseconds
            mHandler.postDelayed(this, 100);
        }
     };
    
  
	
	//----- Recorder ----//
	
	private void startRecording() {
		if( filesManager.isExternalStorageWritable() ){//should check before write
			isRecording = true;
			recorder = new MediaRecorder();

			recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
			recorder.setOutputFormat(AUDIO_OUTPUT_FORMAT);
			recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
			recorder.setOutputFile(filesManager.getAudioFilename());

			recorder.setOnErrorListener(errorListener);
			recorder.setOnInfoListener(infoListener);

			try {
				recorder.prepare();
				recorder.start();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	private void stopRecording() {
		isRecording = false;
		if (null != recorder) {		
			try {
				recorder.stop();
				recorder.reset();
				recorder.release();

				recorder = null;
				
				//reload the playlist
				closePlaylist();
				songsList = filesManager.getPlayList();
				openPlaylist();
            } catch (RuntimeException stopException) {
                // handle cleanup here
                Log.d("Recording Handler", "Runtime Exception");
            }
			
		}
	}

	private MediaRecorder.OnErrorListener errorListener = new MediaRecorder.OnErrorListener() {
		@Override
		public void onError(MediaRecorder mr, int what, int extra) {
			Toast.makeText(MainActivity.this,
					"Error: " + what + ", " + extra, Toast.LENGTH_SHORT).show();
		}
	};

	private MediaRecorder.OnInfoListener infoListener = new MediaRecorder.OnInfoListener() {
		@Override
		public void onInfo(MediaRecorder mr, int what, int extra) {
			Toast.makeText(MainActivity.this,
					"Warning: " + what + ", " + extra, Toast.LENGTH_SHORT)
					.show();
		}
	};
	
    
}
