package com.example.retrofitandroid.activity;

import java.io.File;
import java.io.IOException;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

import com.example.retrofitandroid.BaiLarnService;
import com.example.retrofitandroid.R;
import com.example.retrofitandroid.R.id;
import com.example.retrofitandroid.R.layout;
import com.example.retrofitandroid.R.menu;
import com.example.retrofitandroid.response.Token;
import com.example.retrofitandroid.response.User;
import com.google.gson.Gson;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;

import android.support.v7.app.ActionBarActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	private Token userToken;
	private RestAdapter restAdapter;
	private BaiLarnService service;

	private String api_endpoint;
	private String client_id;
	private String client_secret;
	private String preferences;

	private SharedPreferences sharedPreferences;

	private User userProfile;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		api_endpoint = getString(R.string.api_endpoint);
		client_id = getString(R.string.client_id);
		client_secret = getString(R.string.client_secret);
		preferences = getString(R.string.preferences);

		// OkHttp Cache Setup
		OkHttpClient okHttpClient = new OkHttpClient();
		File cacheDir = getBaseContext().getCacheDir();
		Cache cache = null;
		try {
			cache = new Cache(cacheDir, 1024);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		okHttpClient.setCache(cache);
		// //////////////////

		// Rest Client Setup
		restAdapter = new RestAdapter.Builder()
				.setClient(new OkClient(okHttpClient))
				.setLogLevel(RestAdapter.LogLevel.FULL)
				.setEndpoint(api_endpoint).build();
		service = restAdapter.create(BaiLarnService.class);
		// /////////////////

		// Shared Preferences Setup
		sharedPreferences = getSharedPreferences(preferences,
				Context.MODE_PRIVATE);
		// //////////////

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private boolean resumeHasRun = false;

	protected void onResume() {
		super.onResume();
		// Check for existing token
		if (!sharedPreferences.contains("userToken") & !resumeHasRun) {
			// Go to login page
			resumeHasRun = true;
			Intent myIntent = new Intent(MainActivity.this, LoginActivity.class);
			MainActivity.this.startActivity(myIntent);

		} else if (sharedPreferences.contains("userToken")) {
			Gson gson = new Gson();
			String json = sharedPreferences.getString("userToken", "");
			userToken = gson.fromJson(json, Token.class);

			// Go to user profile page
			Intent myIntent = new Intent(MainActivity.this,
					UserProfileActivity.class);
			MainActivity.this.startActivity(myIntent);
			finish();
		} else {
			finish();
		}
		// //////////////////////
	}
}
