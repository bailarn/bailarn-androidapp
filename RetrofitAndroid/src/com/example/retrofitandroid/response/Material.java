package com.example.retrofitandroid.response;

import com.google.gson.annotations.SerializedName;

public class Material {
	
	//private Course course;
	private String name;
	private String version;
	//private User upload_by;
	//private String file_path;
	
//	public Class get_class() {
//		return _class;
//	}
//	public Course getCourse() {
//		return course;
//	}
	public String getName() {
		return name;
	}
	public String getVersion() {
		return version;
	}
//	public User getUpload_by() {
//		return upload_by;
//	}
//	public String getFile_path() {
//		return file_path;
//	}
	public String toString(){
		return name;
	}
	
}
