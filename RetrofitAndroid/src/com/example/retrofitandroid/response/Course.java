package com.example.retrofitandroid.response;

public class Course {
	private String id;
	private String name;
	private String course_code;
	private int credit;
	private String description;
	private int semester;
	private int year;

	// private User teach_by;
	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCourse_code() {
		return course_code;
	}

	public void setCourse_code(String course_code) {
		this.course_code = course_code;
	}

	public int getCredit() {
		return credit;
	}

	public void setCredit(int credit) {
		this.credit = credit;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getSemester() {
		return semester;
	}

	public void setSemester(int semester) {
		this.semester = semester;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String toString() {
		return course_code + " " + name;
	}

	// public User getTeach_by() {
	// return teach_by;
	// }
	// public void setTeach_by(User teach_by) {
	// this.teach_by = teach_by;
	// }

}
