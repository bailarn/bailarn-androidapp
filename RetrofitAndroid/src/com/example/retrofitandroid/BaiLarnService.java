package com.example.retrofitandroid;

import java.util.List;

import com.example.retrofitandroid.request.TokenRenewalRequest;
import com.example.retrofitandroid.request.UserLoginRequest;
import com.example.retrofitandroid.response.Class;
import com.example.retrofitandroid.response.Course;
import com.example.retrofitandroid.response.Material;
import com.example.retrofitandroid.response.Token;
import com.example.retrofitandroid.response.User;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

public interface BaiLarnService {
	@GET("/api/user")
	void getUserProfile(@Query("access_token") String access_token,
			Callback<User> cb);

	@GET("/api/class/todayclass")
	void getTodayClasses(@Query("access_token") String access_token,
			Callback<List<Class>> cb);

	@GET("/api/course/getcourses")
	void getCourses(@Query("access_token") String access_token,
			Callback<List<Course>> cb);

	@GET("/api/course/getmaterials")
	void getMaterialsFromCourse(@Query("access_token") String access_token,
			@Query("course_id") String course_id, Callback<List<Material>> cb);
	
	@GET("/api/class/getmaterials")
	void getMaterialsFromClass(@Query("access_token") String access_token,
			@Query("class_id") String class_id, Callback<List<Material>> cb);

	@POST("/oauth/token")
	void login(@Body UserLoginRequest credential, Callback<Token> cb);

	@POST("/oauth/token")
	void renewToken(@Body TokenRenewalRequest request, Callback<Token> cb);
}
